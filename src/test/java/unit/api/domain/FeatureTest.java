package unit.api.domain;

import org.junit.Test;

import api.domain.*;

import static org.junit.Assert.*;

import java.util.*;

public class FeatureTest {
  private final String customerId = "d71e7703-b869-4c5d-aaa9-90708df76209";
  
  @Test
  public void shouldBeActiveWithCustomer(){
    Customer customer = new Customer(customerId, "some customer");
    Feature feature = createFakeFeature(false, customer);
    boolean result = feature.isFeatureActiveForCustomer(customer);

    assertTrue(result);
  }

  @Test
  public void shouldBeActiveWithoutCustomerButInverted(){
    Customer customer = new Customer(customerId, "some customer");
    Feature feature = createFakeFeature(true, null);
    boolean result = feature.isFeatureActiveForCustomer(customer);

    assertTrue(result);
  }

  @Test
  public void shouldNotBeActiveWithoutCustomer(){
    Customer customer = new Customer(customerId, "some customer");
    Feature feature = createFakeFeature(false, null);
    boolean result = feature.isFeatureActiveForCustomer(customer);

    assertFalse(result);
  }

  @Test
  public void shouldNotBeActiveWithCustomerButInverted(){
    Customer customer = new Customer(customerId, "some customer");
    Feature feature = createFakeFeature(true, customer);
    boolean result = feature.isFeatureActiveForCustomer(customer);

    assertFalse(result);
  }

  private Feature createFakeFeature(boolean isInverted, Customer customer){
    List<Customer> customers = new ArrayList<Customer>();
    if(customer != null) customers.add(customer);

    return new Feature(null, null, null, null,  isInverted, null, null, null, null, customers);
  }
}