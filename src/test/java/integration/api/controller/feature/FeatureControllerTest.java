package integration.api.controller.feature;

import api.domain.*;
import api.App;
import api.controller.feature.FeatureController;
import api.repository.FeatureRepository;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.ArgumentMatchers;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.CoreMatchers.is;
import java.util.UUID;

@RunWith(SpringRunner.class)
//@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(FeatureController.class)
@ContextConfiguration(classes = App.class)
public class FeatureControllerTest {
  @MockBean
  private FeatureRepository featureRepository;
  @Autowired
  private MockMvc mvc;

  @Test
  public void shouldGet() throws Exception {
      Feature feature = new Feature(UUID.fromString("964b40d2-f536-480d-a5c4-193b8f0ac55f"),"some name", null, null, false, null, null, null, null, null);
  
      Mockito.when(featureRepository.findById(ArgumentMatchers.any(UUID.class)))
        .thenReturn(feature);
  
      mvc.perform(get("/api/v1/feature/964b40d2-f536-480d-a5c4-193b8f0ac55f")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("name", is(feature.getName())));
  }
}