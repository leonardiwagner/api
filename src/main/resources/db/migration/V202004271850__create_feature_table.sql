CREATE TABLE public.feature
(
    id uuid NOT NULL,
    name character varying NOT NULL,
    display_name character varying,
    expiration timestamp without time zone,
    description character varying,
    is_inverted boolean NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone,
    deleted timestamp without time zone,

    PRIMARY KEY (id)
);