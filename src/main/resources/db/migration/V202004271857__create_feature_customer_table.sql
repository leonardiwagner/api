CREATE TABLE public.feature_customer
(
    feature_id uuid NOT NULL,
    customer_id uuid NOT NULL,

    CONSTRAINT fk_feature_id FOREIGN KEY (feature_id)
        REFERENCES public.feature (id),
    CONSTRAINT fk_customer_id FOREIGN KEY (customer_id)
        REFERENCES public.customer (id)
);