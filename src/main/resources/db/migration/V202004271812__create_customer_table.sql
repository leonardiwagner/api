CREATE TABLE customer
(
    id uuid NOT NULL,
    name character varying,
    created timestamp without time zone NOT NULL,

    PRIMARY KEY (id)
);