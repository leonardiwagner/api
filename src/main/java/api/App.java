package api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App {
  public static void main(String[] args) {
    Migration migration = new Migration();
    migration.migrate();
    
    SpringApplication.run(App.class, args);
  }
}
