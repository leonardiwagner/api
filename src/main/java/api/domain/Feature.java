package api.domain;

import java.util.*;
import java.time.LocalDateTime;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import org.hibernate.annotations.GenericGenerator;

import lombok.*;

@Entity
@Builder @Getter @AllArgsConstructor
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Feature {
  @Id @GeneratedValue(generator="UUID2")
  @GenericGenerator(name="UUID2", strategy = "org.hibernate.id.UUIDGenerator")
  final UUID id;
  public final String name;
  final String displayName;
  final String description;
  final boolean isInverted;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  final LocalDateTime expiration;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  final LocalDateTime created;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  final LocalDateTime updated;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  final LocalDateTime deleted;

  @ManyToMany()
  @JoinTable(
    name = "feature_customer", 
    joinColumns = @JoinColumn(name = "feature_id"), 
    inverseJoinColumns = @JoinColumn(name = "customer_id"))
  final List<Customer> customers;

  public Feature(){
    id = null;
    name = null;
    displayName = null;
    description = null;
    expiration = null;
    isInverted = false;
    created = null;
    deleted = null;
    updated = null;
    customers = null;
  }

  private FeatureBuilder fromFeature(Feature feature){
    return Feature.builder()
      .id(id)
      .name(name)
      .displayName(displayName)
      .description(description)
      .expiration(expiration)
      .isInverted(isInverted)
      .customers(customers)
      .created(created)
      .updated(updated)
      .deleted(deleted);
  }

  public Feature create() {
    return fromFeature(this)
      .created(LocalDateTime.now())
      .updated(LocalDateTime.now())
      .build();
  }

  public Feature update(final Feature updatedFeature) {
    return fromFeature(this)
      .id(id)
      .name(updatedFeature.name)
      .displayName(updatedFeature.displayName)
      .description(updatedFeature.description)
      .expiration(updatedFeature.expiration)
      .isInverted(updatedFeature.isInverted)
      .customers(updatedFeature.customers)
      .updated(LocalDateTime.now())
      .build();
  }

  public Feature delete() {
    return fromFeature(this)
      .deleted(LocalDateTime.now())
      .build();
  }

  public boolean isCustomerInFeature(final Customer customer) {
    if (customer == null || customer.id == null)
      return false;

    return this.customers.stream().map((c) -> c.getId()).anyMatch((customerId) -> customer.getId().equals(customerId));
  }

  public boolean isFeatureActiveForCustomer(final Customer customer) {
    boolean customerIsInTheFeature = isCustomerInFeature(customer) && !isInverted;
    boolean customerIsNotInAnInvertedFeature = !isCustomerInFeature(customer) && isInverted;

    return customerIsInTheFeature || customerIsNotInAnInvertedFeature;
  }
}
