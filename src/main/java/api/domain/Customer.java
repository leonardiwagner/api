package api.domain;

import java.util.*;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Customer {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  final UUID id;
  final String name;
  
  @JsonIgnore
  @ManyToMany()
  @JoinTable(
    name = "feature_customer", 
    joinColumns = @JoinColumn(name = "customer_id"), 
    inverseJoinColumns = @JoinColumn(name = "feature_id"))
  final List<Feature> features;

  public Customer() {
    this.id = UUID.randomUUID();
    this.name = "douglinha";
    this.features = new ArrayList<Feature>();
  }
  
  public Customer(String id, String name) {
    this.id = UUID.fromString(id);
    this.name = name;
    this.features = new ArrayList<Feature>();
  }

  public String getId(){
    return id.toString();
  }

  public String getName(){
    return name;
  }

  public Iterable<Feature> getFeatures(){
    return features;
  }
}