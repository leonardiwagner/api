package api;

import org.flywaydb.core.Flyway;

public class Migration {
  public void migrate() {
    Flyway flyway = Flyway.configure()
      .dataSource(System.getenv("API_DATABASE_URL"),
                          System.getenv("API_DATABASE_USER"),
                          System.getenv("API_DATABASE_PASSWORD"))
      .load();
      
    flyway.migrate();
  }
}
