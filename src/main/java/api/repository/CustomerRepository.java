package api.repository;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

import api.domain.*;

public interface CustomerRepository extends CrudRepository<Customer,Long> {
  Customer findById(UUID id);
}