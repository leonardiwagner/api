package api.repository;

import java.util.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import api.domain.*;

@Repository
public interface FeatureRepository extends CrudRepository<Feature,Long> {
  Feature findById(UUID id);
  List<Feature> findByDeletedIsNull();
  List<Feature> findByNameAndDeletedIsNull(String name);
}