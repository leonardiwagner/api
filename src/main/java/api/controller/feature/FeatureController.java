package api.controller.feature;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import api.domain.*;
import api.repository.FeatureRepository;

@RestController
@RequestMapping("/api/v1/feature")
public class FeatureController {
  @Autowired
  private FeatureRepository repository;

  @GetMapping("/{id}")
	public Feature get(@PathVariable String id){
    Feature feature = repository.findById(UUID.fromString(id));
    return feature;
  }

  @PostMapping
  public Feature insert(@RequestBody Feature feature){
    Feature newFeature = feature.create();
    repository.save(newFeature);

    return newFeature;
  }

  @PutMapping("/{id}")
  public Feature update(@PathVariable String id, @RequestBody Feature feature){
    Feature originalFeature = repository.findById(UUID.fromString(id));
    Feature updatedFeature = originalFeature.update(feature);
    
    repository.save(updatedFeature);
    return updatedFeature;
  }

  @DeleteMapping("/{id}")
	public Feature delete(@PathVariable String id){
    Feature originalFeature = repository.findById(UUID.fromString(id));
    Feature deletedFeature = originalFeature.delete();
    
    repository.save(deletedFeature);
    return deletedFeature;
  }
}