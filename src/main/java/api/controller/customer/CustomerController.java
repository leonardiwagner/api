package api.controller.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import api.domain.*;
import api.repository.CustomerRepository;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
  @Autowired
  private CustomerRepository repository;

  @GetMapping
	public Iterable<Customer> get(){
    return repository.findAll();
  }
}