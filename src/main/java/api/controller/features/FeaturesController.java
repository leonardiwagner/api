package api.controller.features;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import api.domain.*;
import api.repository.*;

@RestController
@RequestMapping("/api/v1/features")
public class FeaturesController {
  @Autowired
  FeatureRepository featureRepository;
  @Autowired
  CustomerRepository customerRepository;
  
  @GetMapping
  public List<Feature> get(){
    List<Feature> nonDeletedFeatures = featureRepository.findByDeletedIsNull();
    return nonDeletedFeatures;
  }

  @PostMapping
	public FeaturesResponse get(@RequestBody FeaturesRequest featureRequest){
    List<Feature> featuresFound = createListOfFoundFeatures(featureRequest);
    Customer customer = customerRepository.findById(UUID.fromString(featureRequest.getCustomerId()));
    List<FeatureResponse> featureResponse = createResponse(customer, featuresFound);

		return new FeaturesResponse(featureResponse);
  }

  private List<Feature> createListOfFoundFeatures(FeaturesRequest featureRequest){
    List<Feature> requestedFeatures = featureRequest.getFeatures();
    List<Feature> featuresFound = new ArrayList<Feature>();
    

    for (Feature requestedFeature : requestedFeatures) {
      List<Feature> features = featureRepository.findByNameAndDeletedIsNull(requestedFeature.getName());
      featuresFound.addAll(features);
    }

    return featuresFound;
  }

  private List<FeatureResponse> createResponse(Customer customer, List<Feature> featuresFound){
    List<FeatureResponse> featureResponse = new ArrayList<FeatureResponse>();
    for (Feature feature : featuresFound) {
      boolean active = feature.isFeatureActiveForCustomer(customer);
      featureResponse.add(new FeatureResponse(feature.getName(), active, feature.isInverted()));
    }

    return featureResponse;
  }

}