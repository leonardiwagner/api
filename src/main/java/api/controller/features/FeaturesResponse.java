package api.controller.features;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class FeaturesResponse {
  final List<FeatureResponse> features;

  public FeaturesResponse(List<FeatureResponse> features){
    this.features = features;
  }
}