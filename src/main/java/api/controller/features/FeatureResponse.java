package api.controller.features;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class FeatureResponse {
  final String name;
  final boolean active;
  final boolean inverted;

  public FeatureResponse(String name, boolean active, boolean inverted) {
    this.name = name;
    this.active = active;
    this.inverted = inverted;
  }
}