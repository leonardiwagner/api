package api.controller.features;

import java.util.*;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import api.domain.*;

@JsonTypeName("featureRequest")                                                                                         
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT ,use = JsonTypeInfo.Id.NAME)
public class FeaturesRequest {
  private final String customerId;
  private final List<Feature> features;
  
  private FeaturesRequest(){
    this.customerId = "";
    this.features = new ArrayList<Feature>();
  }

  public FeaturesRequest(String customerId, List<Feature> features) {
    this.customerId = customerId;
    this.features = features;
  }

  public String getCustomerId(){
    return customerId;
  }

  public List<Feature> getFeatures(){
    return features;
  }
}