package api;

import javax.sql.DataSource;
import org.springframework.boot.jdbc.*;
import org.springframework.context.annotation.*;

@Configuration
public class DataSourceConfig {
     
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(System.getenv("API_DATABASE_URL"));
        dataSourceBuilder.username(System.getenv("API_DATABASE_USER"));
        dataSourceBuilder.password(System.getenv("API_DATABASE_PASSWORD"));
        return dataSourceBuilder.build();
    }
}