# API ![api build status](https://codebuild.us-west-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiYXg3RnpveXBLY1AreU5wWUU4ckowMXlFTjRBL3NIaWx2eDFJSHJDY1FsdG1RMGxZTUFDNXRJdHNtbVkxRy9jWGhrcXdxbnFJVE9GRGtWYXNZbDZtVmprPSIsIml2UGFyYW1ldGVyU3BlYyI6Im5mcmNITW1qTks3S2tXQnoiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

This project is written with Java 8 meant to be the REST API for the feature toggle web application  

# How to run manually
- Install JDK 8 ([Link](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)).
- Install Gradle 6.3 ([Link](https://gradle.org/install/)).
- set PostgreSQL environment variables:
    - `API_DATABASE_URL`: PorsgreSQL address with database name example: `jdbc:postgresql://db:5432/assessment`.
    - `API_DATABASE_USER`: database username.
    - `API_DATABASE_PASSWORD`: database password.
- run `gradle flywayMigrate` to run database migrations.
- run `gradle run` to start the project. Default port is `8080`.

# Features REST API

Query feature status by a given `customerId` and features `name`.

>**Important:** `name` property is described as *Technical Name* in the website, not *Display Name*.

- Address: `/api/v1/features`.
- METHOD: `POST`.
- Request Body *application/JSON* , example:

  
```json
{
  "featureRequest": {
    "customerId": "cd86b458-3cc2-4266-87c9-61befc685684",
    "features": [
        {"name": "Feature A"},
        {"name": "Feature B"},
    ]
  }
}
```

- Response Example:


```json
{
  "features": [
    { "name": "Feature A", "active": true, "inverted": false },
    { "name": "Feature B", "active": false, "inverted": true }
  ]
}
```
